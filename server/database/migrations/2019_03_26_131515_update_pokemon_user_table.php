<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePokemonUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pokemon_user', function (Blueprint $table) {

            $table->foreign('pokemon_id')
                ->references('id')
                ->on('pokemon');

            $table->foreign('user_id')
                ->references('id')
                ->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pokemon_user', function (Blueprint $table) {
            $table->dropForeign(['pokemon_id']);
            $table->dropForeign(['user_id']);
        });
    }
}
