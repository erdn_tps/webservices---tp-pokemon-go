<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePokemonTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pokemon_type', function (Blueprint $table) {

            $table->foreign('pokemon_id')
                ->references('id')
                ->on('pokemon');

            $table->foreign('type_id')
                ->references('id')
                ->on('types');

            $table->unique(['pokemon_id', 'type_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pokemon_user', function (Blueprint $table) {
            $table->dropUnique(['pokemon_id', 'type_id']);
            $table->dropForeign(['type_id']);
            $table->dropForeign(['pokemon_id']);
        });
    }
}
