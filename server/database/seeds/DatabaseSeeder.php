<?php

use Illuminate\Database\Seeder;
use App\Type;
use App\Pokemon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        (new Type)->setName('Insecte')->save();     // 1
        (new Type)->setName('Ténèbre')->save();     // 2
        (new Type)->setName('Dragon')->save();      // 3
        (new Type)->setName('Electric')->save();    // 4
        (new Type)->setName('Fée')->save();         // 5
        (new Type)->setName('Combat')->save();      // 6
        (new Type)->setName('Feu')->save();         // 7
        (new Type)->setName('Vol')->save();         // 8
        (new Type)->setName('Spectre')->save();     // 9
        (new Type)->setName('Plante')->save();      // 10
        (new Type)->setName('Sol')->save();         // 11
        (new Type)->setName('Glace')->save();       // 12
        (new Type)->setName('Normal')->save();      // 13
        (new Type)->setName('Poison')->save();      // 14
        (new Type)->setName('Psy')->save();         // 15
        (new Type)->setName('Roche')->save();       // 16
        (new Type)->setName('Acier')->save();       // 17
        (new Type)->setName('Eau')->save();         // 18

        (new Pokemon)->setName('Bulbizarre')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Herbizarre')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Florizarre')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Salamèche')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Reptincel')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Dracaufeu')->saveWithChain()->setTypes([7, 8]);
        (new Pokemon)->setName('Carapuce')->saveWithChain()->setTypes([18]);
        (new Pokemon)->setName('Carabaffe')->saveWithChain()->setTypes([18]);
        (new Pokemon)->setName('Tortank')->saveWithChain()->setTypes([18]);
        (new Pokemon)->setName('Chenipan')->saveWithChain()->setTypes([1]);
        (new Pokemon)->setName('Chrysacier')->saveWithChain()->setTypes([1]);
        (new Pokemon)->setName('Papilusion')->saveWithChain()->setTypes([1, 8]);
        (new Pokemon)->setName('Aspicot')->saveWithChain()->setTypes([1, 14]);
        (new Pokemon)->setName('Coconfort')->saveWithChain()->setTypes([1, 14]);
        (new Pokemon)->setName('Dardargnan')->saveWithChain()->setTypes([1, 14]);
        (new Pokemon)->setName('Roucool')->saveWithChain()->setTypes([13, 8]);
        (new Pokemon)->setName('Roucoups')->saveWithChain()->setTypes([13, 8]);
        (new Pokemon)->setName('Roucarnage')->saveWithChain()->setTypes([13, 8]);
        (new Pokemon)->setName('Rattata')->saveWithChain()->setTypes([13]);
        (new Pokemon)->setName('Rattatac')->saveWithChain()->setTypes([13]);
        (new Pokemon)->setName('Piafabec')->saveWithChain()->setTypes([13, 8]);
        (new Pokemon)->setName('Rapasdepic')->saveWithChain()->setTypes([13, 8]);
        (new Pokemon)->setName('Abo')->saveWithChain()->setTypes([14]);
        (new Pokemon)->setName('Arbok')->saveWithChain()->setTypes([14]);
        (new Pokemon)->setName('Pikachu')->saveWithChain()->setTypes([4]);
        (new Pokemon)->setName('Raichu')->saveWithChain()->setTypes([4]);
        (new Pokemon)->setName('Sabelette')->saveWithChain()->setTypes([11]);
        (new Pokemon)->setName('Sablaireau')->saveWithChain()->setTypes([11]);
        (new Pokemon)->setName('Nidoran F')->saveWithChain()->setTypes([14]);
        (new Pokemon)->setName('Nidorina')->saveWithChain()->setTypes([14]);
        (new Pokemon)->setName('Nidoqueen')->saveWithChain()->setTypes([14, 11]);
        (new Pokemon)->setName('Nidoran M')->saveWithChain()->setTypes([14]);
        (new Pokemon)->setName('Nidorino')->saveWithChain()->setTypes([14]);
        (new Pokemon)->setName('Nidoking')->saveWithChain()->setTypes([14, 11]);
        (new Pokemon)->setName('Melofee')->saveWithChain()->setTypes([5]);
        (new Pokemon)->setName('Melodelfe')->saveWithChain()->setTypes([5]);
        (new Pokemon)->setName('Goupix')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Feunard')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Rondoudou')->saveWithChain()->setTypes([13, 5]);
        (new Pokemon)->setName('Grodoudou')->saveWithChain()->setTypes([13, 5]);
        (new Pokemon)->setName('Nosferapti')->saveWithChain()->setTypes([14, 8]);
        (new Pokemon)->setName('Nosferalto')->saveWithChain()->setTypes([14, 8]);
        (new Pokemon)->setName('Mystherbe')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Ortide')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Rafflesia')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Paras')->saveWithChain()->setTypes([1, 10]);
        (new Pokemon)->setName('Parasect')->saveWithChain()->setTypes([1, 10]);
        (new Pokemon)->setName('Mimitos')->saveWithChain()->setTypes([1, 14]);
        (new Pokemon)->setName('Aeromite')->saveWithChain()->setTypes([1, 14]);
        (new Pokemon)->setName('Taupiqueur')->saveWithChain()->setTypes([11]);
        (new Pokemon)->setName('Triopikeur')->saveWithChain()->setTypes([11]);
        (new Pokemon)->setName('Miaouss')->saveWithChain()->setTypes([13]);
        (new Pokemon)->setName('Persian')->saveWithChain()->setTypes([13]);
        (new Pokemon)->setName('Psykokwak')->saveWithChain()->setTypes([18]);
        (new Pokemon)->setName('Akwakwak')->saveWithChain()->setTypes([18]);
        (new Pokemon)->setName('Ferosinge')->saveWithChain()->setTypes([6]);
        (new Pokemon)->setName('Colosinge')->saveWithChain()->setTypes([6]);
        (new Pokemon)->setName('Caninos')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Arcanin')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Ptitard')->saveWithChain()->setTypes([18]);
        (new Pokemon)->setName('Tetarte')->saveWithChain()->setTypes([18]);
        (new Pokemon)->setName('Tartard')->saveWithChain()->setTypes([18, 6]);
        (new Pokemon)->setName('Abra')->saveWithChain()->setTypes([15]);
        (new Pokemon)->setName('Kadabra')->saveWithChain()->setTypes([15]);
        (new Pokemon)->setName('Alakazam')->saveWithChain()->setTypes([15]);
        (new Pokemon)->setName('Machoc')->saveWithChain()->setTypes([6]);
        (new Pokemon)->setName('Machopeur')->saveWithChain()->setTypes([6]);
        (new Pokemon)->setName('Mackogneur')->saveWithChain()->setTypes([6]);
        (new Pokemon)->setName('Chetiflor')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Boustiflor')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Empiflor')->saveWithChain()->setTypes([10, 14]);
        (new Pokemon)->setName('Tentacool')->saveWithChain()->setTypes([18, 14]);
        (new Pokemon)->setName('Tentacruel')->saveWithChain()->setTypes([18, 14]);
        (new Pokemon)->setName('Racaillou')->saveWithChain()->setTypes([16, 11]);
        (new Pokemon)->setName('Gravalanch')->saveWithChain()->setTypes([16, 11]);
        (new Pokemon)->setName('Grolem')->saveWithChain()->setTypes([16, 11]);
        (new Pokemon)->setName('Ponyta')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Galopa')->saveWithChain()->setTypes([7]);
        (new Pokemon)->setName('Ramoloss')->saveWithChain()->setTypes([18, 15]);
        (new Pokemon)->setName('Flagadoss')->saveWithChain()->setTypes([18, 15]);
        (new Pokemon)->setName('Magneti')->saveWithChain()->setTypes([4, 17]);
        (new Pokemon)->setName('Magneton')->saveWithChain()->setTypes([4, 17]);

    }
}
