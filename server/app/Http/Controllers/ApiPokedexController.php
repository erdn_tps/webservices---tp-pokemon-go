<?php

namespace App\Http\Controllers;

use App\Pokemonbox;
use Illuminate\Http\Request;

class ApiPokedexController extends Controller
{
    public function index()
    {
        return response( Pokemonbox::with('pokemon')->get() )
            ->header('Access-Control-Allow-Origin', '*');
    }
}
