<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    function setName( string $name ): self{
        $this->name = $name;
        return $this;
    }

    // un type appartient à plusieurs pokemons
    function pokemons() {
        return $this->belongsToMany(Pokemon::class);
    }

    // un type peut avoir plusieurs event
    function events() {
        return $this->hasMany(Event::class);
    }
}
