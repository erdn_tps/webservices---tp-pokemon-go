<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    // un event appartient à un type
    function types() {
        return $this->belongsTo(Type::class);
    }
}
