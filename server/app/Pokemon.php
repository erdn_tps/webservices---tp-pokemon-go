<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{

    function saveWithChain(): self {
        $this->save();
        return $this;
    }

    function setName( string $name ): self{
        $this->name = $name;
        return $this;
    }

    function setTypes( array $types ): self{
        foreach ( $types as $id_type ) {
            $this->types()->attach( $id_type );
        }
        return $this;
    }

    // un pokemon appartient à plusieurs types
    function types() {
        return $this->belongsToMany(Type::class);
    }

    // un pokemon peut appartenir à plusieurs pokemonbox
    function pokemonbox() {
        return $this->belongsToMany(Pokemonbox::class);
    }

}
