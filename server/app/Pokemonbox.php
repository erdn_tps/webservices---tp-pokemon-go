<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemonbox extends Model
{
    protected $table = 'pokemon_user';

    protected $fillable = [
        'pokemon_id', 'user_id', 'pc'
    ];

    // une boite pokemon appartient à un seul utilisateur
    function users() {
        return $this->belongsTo(User::class);
    }

    // un pokemon appartient à une seul espèce
    function pokemon() {
        // on lui passe la foreignKey et la localKey pour qu'il puisse faire le lien correctement
        return$this->hasOne(Pokemon::class, 'id', 'pokemon_id');
    }
}
