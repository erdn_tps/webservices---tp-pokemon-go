const path = require('path');

const CONFIG = {

    mode: 'development',

    devtool: 'inline-source-map',

    entry: './app/js/app.js',
    output: {
        path: path.resolve( __dirname, 'dist' ),
        filename: 'bundle.js'
    },

    resolve: {
        extensions: ['.js', '.json', '.scss', '.html']
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    {
                        'loader': 'css-loader',
                        options: { sourceMap: true }
                    },
                    {
                        'loader': 'sass-loader',
                        options: { sourceMap: true }
                    }
                ]
            },
            {
                test: /\.html$/,
                use: 'html-loader'
            },
            {
                test: /\.(png|jpg|jpeg)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        outputPath: 'markers'
                    }
                }
            },
            {
                test: /\.gif$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        outputPath: 'images'
                    }
                }
            }
        ]
    },

    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port:9000
    }

};

module.exports = (function(env){

    //Récupère le --env de la command line
    if( env && env.production == 'true' ) {
        CONFIG.mode = 'production';
        CONFIG.devtool = false;
    }

    return CONFIG;

});

//npm start : lancer le serveur de developpement
//npm run prod : générer les fichier minifiés de production
//npm run dev : générer les fichiers non minifiés