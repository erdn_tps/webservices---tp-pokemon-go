import TYPE from "../TYPE";

export class Event {

    constructor( start_event, end_event, type_event) {

        this.start_event = start_event;
        this.end_event = end_event;
        this.type_event = type_event;

        this.html = null;

        this.$list_events = document.getElementById('events');
        this.$event_img = null;

    }

    render() {

        this.html = `
            <div class="event-wrap">
                <div id="event"></div>
                <div class="detail">
                    <div>Type: ${TYPE[this.type_event]}</div>
                    <div>Date début: ${this.start_event}</div>
                    <div>Date fin: ${this.end_event}</div>
                </div>            
            </div>
            `

        this.$list_events.innerHTML = this.html;

        this.$event_img = document.getElementById('event');


    }

    bgn_img() {
        this.$event_img.style.backgroundPositionX = `${this.type_event * -83}px`;
    }

}
