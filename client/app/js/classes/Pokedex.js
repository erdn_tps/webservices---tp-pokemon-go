import trainer from './Trainer';
import CONFIG from "../CONFIG";
import pokedexService from "../services/PokedexService";


class Pokedex {

    constructor() {

        this.$list = document.getElementById('list');

    }

    render() {

        const url = `${CONFIG.SERVER}/api/pokedex`;

        pokedexService.fetchAll( url )
            .then( pokemons => {

                let html = ``;

                for( let pokemon of pokemons ) {

                    const posX = (( pokemon.id - 1 ) % 25 ) * 100 * -1;
                    const posY = Math.floor((pokemon.id - 1) / 25) * 100 * -1;

                    html += `
                        <div class="pokemon-detail">
                            <span class="pc"> ${pokemon.pc} PC</span>
                            <div class="sprite pokemon active" 
                                style="background-position-x: ${posX}px; background-position-y: ${posY}px;"></div>
                            <span class="name">${pokemon.name}</span> 
                        </div>
                    `

                }

                this.$list.innerHTML = html;

            } );

    }

}

export default new Pokedex;