import {Tools} from "./Tools";
import pokemonService from "../services/PokemonService";


class Trainer {

    constructor() {
        this.pokedex = [];
    }

    catch( pokemon ){
        return pokemonService.save(pokemon);
    }

    seePokemon( pokemon, position ) {

        if ( Tools.radar( position.lat, position.lng,
            pokemon.position.lat, pokemon.position.lng ) <= 0.5 ) {
                pokemon.$marker.classList.add('active');
            }
        else {
            pokemon.$marker.classList.remove('active');
        }

    }

}

export default new Trainer;