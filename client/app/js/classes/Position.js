import {Tools} from "./Tools";

export class Position {

    constructor( lat, lng ) {
        this.lat = lat;
        this.lng = lng;
    }

    randomLat( min, max ) {
        this.lat = Tools.getRandomFloat(min, max);

        return this;
    }

    randomLng( min, max ) {
        this.lng = Tools.getRandomFloat(min, max);

        return this;
    }

}