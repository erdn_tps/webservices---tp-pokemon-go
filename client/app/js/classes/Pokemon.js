import mapboxgl from "mapbox-gl";
import game from './Game';
import trainer from './Trainer';

export class Pokemon {

    constructor( id, name, types, pc, position = [0, 0] ) {

        this.$popup = null;
        this.$button = null;

        this.id = id;
        this.name = name;
        this.types = types;
        this.pc = pc;
        this.position = position;

        this.$marker = null;
        this.marker = null;
        this.popup = new mapboxgl.Popup({
            offset: 25
        });

    }

    createPopup() {

        this.$popup = document.createElement('div');
        let html = `<h1> ${this.name} </h1><ul>`;
            for ( let type of this.types ) {

                html += `<li>${type.name}</li>`

            }
        html += `</ul> <h3> ${this.pc} PC</h3>`;

        this.$popup.innerHTML = html;

        this.$button = document.createElement('button');
        this.$button.innerText = 'Capturer';

        this.$popup.append( this.$button );

        this.popup
            .setDOMContent( this.$popup );

    }

    catchHim() {

        this.$button.addEventListener('click', () => {

            // on empêche le clique sur le bouton lors de l'attente de la réponse
            this.$button.setAttribute('disabled', 'disabled');
            trainer.catch(this)
                .then( () => {
                    this.destroy();
                    for( let key in game.listPokemons){
                        game.listPokemons.splice(key, 1);
                    }
            });

        });

    }

    render( map ) {
        this.createPopup();
        this.catchHim();

        this.$marker = document.createElement('div');
        this.$marker.className = 'sprite pokemon';
        const posX = (( this.id - 1 ) % 25 ) * 100 * -1;
        const posY = Math.floor((this.id - 1) / 25) * 100 * -1;
        this.$marker.style.backgroundPositionX = `${posX}px`;
        this.$marker.style.backgroundPositionY = `${posY}px`;
        this.marker = new mapboxgl.Marker(this.$marker);

        this.marker
            .setLngLat( this.position )
            .setPopup( this.popup )
            .addTo( map );
    }

    destroy() {
        this.marker.remove();
    }

}