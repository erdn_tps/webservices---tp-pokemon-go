import mapboxgl from 'mapbox-gl/dist/mapbox-gl.js';
import config from "../../config";
import {Pokemon} from "./Pokemon";
import {Tools} from "./Tools";
import {Position} from "./Position";
import trainer from "./Trainer";
import pokedex from "./Pokedex";
import CONFIG from "../CONFIG";
import gameService from "../services/GameService";


class Game {

    constructor() {

        this.$loader = document.getElementById('loader');

        this.$pokedex = document.getElementById('pokedex');
        this.$pokebox = document.getElementById('pokemonbox');
        this.$close = document.getElementById('close');

        this.$form = document.getElementById('form-events');
        this.$input_start = document.getElementById('start-event');
        this.$input_end = document.getElementById('end-event');
        this.$select_type = document.getElementById('type-event');

        this.listPokemons = [];

        this.map = null;
        this.initMap();

        const $el = document.createElement('div');
        $el.className = `sprite trainer`;
        this.me = new mapboxgl.Marker( $el );

        this.bindEvents();

    }

    initMap() {

        mapboxgl.accessToken = config.accessToken;
        this.map = new mapboxgl.Map({
            container: config.container,
            style: config.style
        });

    }

    // methode pour faire apparaitre un pokemon aléatoire
    spawn(map) {

        const url = `${CONFIG.SERVER}/api/pokemon`;

        for( let i = 0; i < 1; i ++ ) {

            const position = new Position( 0, 0 );
            position
                .randomLat( config.min_lat, config.max_lat )
                .randomLng( config.min_lng, config.max_lng);

            gameService.fetchAll(url)
                .then( pokemons => {

                    const randomIndex = Tools.getRandomInt(0, pokemons.length - 1);
                    const findPokemon = pokemons[randomIndex];
                    const randomPc = Tools.getRandomInt(10, 2000);
                    const pokemon = new Pokemon( findPokemon.id, findPokemon.name, findPokemon.types, randomPc, position );

                    pokemon.render( map );
                    this.add(pokemon);

                    trainer.seePokemon( pokemon, { lat:42.6822173, lng: 2.7947026999999998 } );

                });

        }



    }

    // methode pour push un objet dans le tableau des pokemons disponibles sur la map
    add( pokemon ) {
        this.listPokemons.push( pokemon );
    }

    // methode pour retirer un objet dans le tableau des pokemons disponibles sur la map
    remove() {
        this.listPokemons.splice( 0, 1 );
    }

    // methode pour afficher le pokedex
    showPokebox() {
        pokedex.render();
        this.$pokebox.classList.add('active');
    }

    // methode pour fermer le pokedex
    hidePokebox() {
        this.$pokebox.classList.remove('active');
    }

    // methode pour lier le bouton à l'action de fermeture
    bindEvents() {

        this.$close.addEventListener('click', () => {

            this.hidePokebox();

        });

    }

}

export default new Game;