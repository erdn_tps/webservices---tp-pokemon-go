const TYPE = {
    0: "insecte",
    1: "ténèbre",
    2: "dragon",
    3: "electrik",
    4: "fée",
    5: "combat",
    6: "feu",
    7: "vol",
    8: "spectre",
    9: "plante",
    10: "sol",
    11: "glace",
    12: "normal",
    13: "poison",
    14: "psy",
    15: "roche",
    16: "acier",
    17: "eau"
}

export default TYPE;