import CONFIG from "../CONFIG";
import {Pokemon} from "../classes/Pokemon";

class GameService {

    constructor() {
        this.allPokemons = [];
    }

    // méthode pour récupérer la liste de tout les pokemons dans un tableau
    fetchAll( url ) {

        // on vérifie que le tableau de tout les pokemons est vide pour ne pas faire de requete inutiles
        if( this.allPokemons.length == 0 ) {

            return fetch( url )
                .then( response => response.json() )
                .then( (datas) => {

                    this.allPokemons = [];
                    for( let data of datas ) {

                        const pokemon = new Pokemon( data.id, data.name, data.types );
                        this.allPokemons.push( pokemon );

                    }
                    return this.allPokemons;

                });

        }
        else {
            // renvoi une promesse qui se résout instantanément
            return Promise.resolve(this.allPokemons);
        }

    }

    // méthode pour trouver un pokemon par son id
    find( pokemon ) {

        const url = `${CONFIG.SERVER}/api/pokemon/${pokemon}`;
        return fetch( url )
            .then(response => response.json() )
            .then( (data)  => {

                const pokemon = new Pokemon( data.id, data.name, data.types );

                return pokemon;

            } );
    }

}

export default new GameService;