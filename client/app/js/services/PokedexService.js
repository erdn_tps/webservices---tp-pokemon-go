import {Pokemon} from "../classes/Pokemon";

class PokedexService {

    fetchAll( url ) {

        return fetch( url )
            .then( response => response.json() )
            .then( datas => {

                const pokemons = [];
                for( let data of datas ) {

                    const pokemon = new Pokemon( data.pokemon.id, data.pokemon.name, data.pokemon.types, data.pc );
                    pokemons.push( pokemon );

                }
                return pokemons;

            });

    }

}

export default new PokedexService;