import CONFIG from "../CONFIG";

class PokemonService {

    save( pokemon ) {

        const url = `${CONFIG.SERVER}/api/pokemon/caught`;

        const form = new FormData;
        form.append('pokemon_id', pokemon.id);
        form.append('user_id', 1);
        form.append('pc', pokemon.pc);

        return fetch( url, {
            method: 'POST',
            body: form
        } )

    }

}

export default new PokemonService;