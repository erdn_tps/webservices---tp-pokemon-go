import '../scss/styles';
import game from './classes/Game';
import {Position} from "./classes/Position";
import {Tools} from "./classes/Tools";
import {Event} from "./classes/Event";
import trainer from "./classes/Trainer";

game.map.on('load', () => {

    game.$loader.classList.add('fade');
    setTimeout(() => {
        game.$loader.classList.add('hide');
    }, 500);

});

navigator
    .geolocation
    .watchPosition( function( geopos ) {

        const position =  new Position(
            geopos.coords.latitude,
            geopos.coords.longitude
        )

        game.map
            .setZoom(15)
            .panTo( position );

        game.me
            .setLngLat( position )
            .addTo( game.map );

        for( let pokemon of game.listPokemons ){
            trainer.seePokemon( pokemon, position );
        }
    }
);

setInterval(() => {

    if(game.listPokemons.length <=25) {

        game.spawn( game.map );

    }

}, 2000);

setInterval(() => {

    if(game.listPokemons.length >= 5) {
        const pokemon = game.listPokemons[0];
        game.remove(0);
        pokemon.destroy();
    }

}, Tools.getRandomInt(3000, 6000));

game.$pokedex.addEventListener('click', () => {

    game.showPokebox();

});

game.$form.addEventListener('submit', (e) => {

    e.preventDefault();

    const start_date = game.$input_start.value;
    const end_date = game.$input_end.value;
    const type = game.$select_type.value;

    const event = new Event( start_date, end_date, type );

    event.render();
    event.bgn_img();

});

